import axios from "axios";
// import authService from "@/services/auth.service";

const currentUrl = window.location.href;
const urlObject = new URL(currentUrl);
const hostWithoutPort = urlObject.hostname;

const instance = axios.create({
  baseURL: `http://${hostWithoutPort}:8081/api`,
  headers: {
    "Content-Type": "application/json",
  },
});


instance.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      // Проверяем код ответа
      if (error.response && error.response.status === 403) {
        handleLogout();
      }

      return Promise.reject(error);
    }
);

const handleLogout = () => {
    alert("Что-то пошло не так, попробуйте выйти и войти");

  // authService.logout();
};

export default instance;
