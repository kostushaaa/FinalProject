import api from './api';

class UserService {
  getUserDetails() {
    return api.get('/user/details');
  }
  getWalletDetails() {
    return api.get('/wallet/get-wallet-details')
  }
}

export default new UserService();
