import {createWebHistory, createRouter} from "vue-router";
import Home from "./components/views/Home.vue";
import Login from "./components/Login.vue";
import Register from "./components/Register.vue";
import GamesView from "@/components/views/Games.vue";
import News from "@/components/views/News.vue";
import Profile from "@/components/Profile.vue";
import FruitComponent from "@/components/components/FruitComponent.vue";
import AddGameComponent from "@/components/components/AddGameComponent.vue";
import NotFound from "@/components/components/errors/404.vue"
import Forbidden from "@/components/components/errors/403.vue"
import MyPurchases from "@/components/views/MyPurchases.vue";


const routes = [
    {
        path: "/",
        name: "home",
        component: Home,
    },
    {
        path: "/home",
        component: Home,
    },
    {
        path: "/login",
        component: Login,
    },
    {
        path: "/register",
        component: Register,
    },
    {
        path: "/profile",
        name: "profile",
        component: Profile,
    },
    {
        path: "/games",
        component: GamesView,
    },
    {
        path: "/news",
        component: News,
    },
    {
        path: "/magic",
        component: FruitComponent,
    },
    {
        path: "/admin",
        component: AddGameComponent,
    },
    {
      path: "/my-purchases",
      component: MyPurchases,
    },
    {
        path: '/404',
        component: NotFound
    },
    {
        path: '/403',
        component: Forbidden
    },
    {
        path: '/:pathMatch(.*)',
        redirect: '/404'
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    const publicPages = [
        '/',
        '/home',
        '/login',
        '/register',
        '/news',
        '/blog',
        '/games',
        '/magic',
    ];



    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');

    // trying to access a restricted page + not logged in
    // redirect to login page
    if (authRequired && !loggedIn) {
        next('/login');
    } else {
        next();
    }
});

export default router;