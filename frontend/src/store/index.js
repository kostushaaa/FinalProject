import { createStore } from "vuex";
import { auth } from "./auth.module";
import UserService from "@/services/user.service";

const store = createStore({
    modules: {
        auth,
    },
    state: {
        footerNotAllowedList: ["/login", "/register"],
        SocialIconsAllowedList: ["/home", "/news", "/", "/games"],
        userDetails: {
            userId: "",
            username: "",
            firstname: "",
            lastname: "",
            email: "",
            role: "",
        },
        walletDetails: {
            amount: "",
        }
    },
    mutations: {
        setUserDetails(state, userDetails) {
            state.userDetails.userId = userDetails.userId;
            state.userDetails.username = userDetails.username;
            state.userDetails.firstname = userDetails.firstname;
            state.userDetails.lastname = userDetails.lastname;
            state.userDetails.email = userDetails.email;
            state.userDetails.role = userDetails.role;
        },
        setWalletDetails(state, amount) {
            state.walletDetails.amount = amount;
        }
    },
    getters: {
        getUserDetails: (state) => {
            return state.userDetails;
        },

        getWalletDetails: (state) => {
            return state.walletDetails;
        }
    },
    actions: {
        async fetchUserDetails(context) {
            try {
                const response = await UserService.getUserDetails();
                context.commit("setUserDetails", response.data);
            } catch (error) {
                console.error("Error fetching user details:", error);
            }
        },

        async fetchWalletDetails(context) {
            try {
                const response = await UserService.getWalletDetails();
                context.commit("setWalletDetails", response.data);
            } catch (error) {
                console.error("Error fetching wallet details:", error);
            }
        }
    },
});

export default store;
