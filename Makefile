# Определение переменных
DOCKER_COMPOSE = docker-compose
APP_NAME = ventorspace
JAR_NAME = backend-0.0.1-SNAPSHOT
MAVEN = mvn

# Сборка приложения
.PHONY: build
build:
	cd backend && $(MAVEN) clean install

# Запуск базы данных
.PHONY: start-db
start-db:
	cd docker && $(DOCKER_COMPOSE) up -d

# Остановка базы данных
.PHONY: stop-db
stop-db:
	cd docker && $(DOCKER_COMPOSE) down

# Запуск бэкенда
.PHONY: start-backend
start-backend: build
	cd backend && nohup java -jar target/$(JAR_NAME).jar > output.log 2>&1 &

# Остановка бэкенда
.PHONY: stop-backend
stop-backend:
	 pkill -f backend/target/backend-0.0.1-SNAPSHOT.jar

# Запуск фронтенда
.PHONY: start-frontend
start-frontend:
	cd frontend && npm install && npm run serve

# Остановка фронтенда
.PHONY: stop-frontend
stop-frontend:
	cd frontend && pkill -f "npm run serve"

# Запуск всего приложения в режиме разработки
.PHONY: run-dev
run-dev: start-db start-backend start-frontend

# Остановка всего приложения и базы данных
.PHONY: stop
stop: stop-db stop-backend stop-frontend

# Очистка сборки
.PHONY: clean
clean:
	$(MAVEN) clean
