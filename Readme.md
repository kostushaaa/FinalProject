markdown
# Ventorspace

Ventorspace is a Java Spring Boot project with Spring Security and JWT authentication on the backend, and Vue.js on the frontend.

## Prerequisites

Before you begin, ensure you have the following installed:

- [Node.js](https://nodejs.org/) >= 14
- [Java](https://www.oracle.com/java/technologies/javase-downloads.html) >= 21
- [Docker Compose](https://docs.docker.com/compose/install/)

## Getting Started

To run the project, use the following command:

```bash
make run-dev
```

This command will build and start the necessary components, including the backend and frontend.

## Project Structure

- **backend**: Java Spring Boot backend with Spring Security and JWT authentication.
- **frontend**: Vue.js frontend.

## Development Setup

### Backend

1. Navigate to the `backend` directory.
2. Run the following command to build the backend:

```bash
./mvnw clean install
```

3. Start the backend with Docker Compose:

```bash
cd ../ && make run-dev
```

### Frontend

1. Navigate to the `frontend` directory.
2. Install the required dependencies:

```bash
npm install
```

3. Start the frontend development server:

```bash
npm run serve
```

4. The frontend will be accessible at `http://localhost:8080`.

## Additional Commands

- **Stop the project (backend and frontend):**

```bash
make stop
```

- **Stop only the backend:**

```bash
make stop-backend
```

- **Clean the project:**

```bash
make clean
```

## Contributing

If you would like to contribute to this project, please follow our [contribution guidelines](CONTRIBUTING.md).

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.