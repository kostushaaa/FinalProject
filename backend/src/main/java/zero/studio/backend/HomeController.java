package zero.studio.backend;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/public")
public class HomeController {
    @GetMapping("/try")
    public ResponseEntity greetFromUnsecuredController() {
        return ResponseEntity.ok("greetFromUnsecuredController");
    }
}
