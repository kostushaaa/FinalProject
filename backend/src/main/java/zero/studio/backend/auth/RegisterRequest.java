package zero.studio.backend.auth;

import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.UniqueElements;
import zero.studio.backend.user.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {
  @NotNull
  private String username;

  @UniqueElements
  private String email;

  @NotNull
  private String password;

  @NotNull
  private Role role;
}
