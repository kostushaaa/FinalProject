package zero.studio.backend.auth;

import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import zero.studio.backend.config.JwtService;
import zero.studio.backend.token.*;
import zero.studio.backend.user.Role;
import zero.studio.backend.user.User;
import zero.studio.backend.user.UserRepository;
import zero.studio.backend.wallet.Wallet;
import zero.studio.backend.wallet.WalletRepository;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository userRepository;
    private final TokenRepository tokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final WalletRepository walletRepository;

    public AuthenticationResponse register(RegisterRequest request) {
        var userEmail = request.getEmail().toLowerCase();
        var username = request.getUsername().toLowerCase();

        Role defaultRole = Role.USER;

        if (userRepository.findByEmail(userEmail).isPresent()) {
            throw new IllegalStateException("Пользователь с таким email уже существует.");
        }

        if (userRepository.findByUsername(username).isPresent()) {
            throw new IllegalStateException("Пользователь с таким nickName уже существует.");
        }

        var user = User
                .builder()
                .username(request.getUsername())
                .firstname(null)
                .lastname(null)
                .email(userEmail)
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole() != null ? request.getRole() : defaultRole)
                .build();

        var wallet = Wallet.builder()
                .balance(0.0)
                .build();

        wallet.setOwner(user);

        var savedUser = userRepository.save(user);

        var jwtToken = jwtService.generateToken(savedUser);
        var refreshToken = jwtService.generateRefreshToken(savedUser);
        saveUserToken(savedUser, jwtToken);

        return AuthenticationResponse.builder().accessToken(jwtToken).refreshToken(refreshToken).username(username).role(user.getRole()).build();
    }


    public AuthenticationResponse authenticate(AuthenticationRequest request) {

        if (request.getUsername() != null) {
            Optional<User> emailOptional = userRepository.findByUsername(request.getUsername());
            if (userRepository.findByUsername(request.getUsername()).isPresent()) {
                request.setEmail(emailOptional.get().getEmail());
            }
        }

        if (request.getEmail() == null) {
            throw new IllegalStateException("In request for authenticate required Email");
        }

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

        var user = userRepository.findByEmail(request.getEmail()).orElseThrow();
        var jwtToken = jwtService.generateToken(user);
        var refreshToken = jwtService.generateRefreshToken(user);
        revokeAllUserTokens(user);
        saveUserToken(user, jwtToken);
        return AuthenticationResponse.builder().accessToken(jwtToken).refreshToken(refreshToken).build();
    }

    private void saveUserToken(User user, String jwtToken) {
        var token = Token.builder().user(user).token(jwtToken).tokenType(TokenType.BEARER).expired(false).revoked(false).build();
        tokenRepository.save(token);
    }

    private void revokeAllUserTokens(User user) {
        var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
        if (validUserTokens.isEmpty()) return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }

    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {

        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String userEmail;

        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            return;
        }
        refreshToken = authHeader.substring(7);
        userEmail = jwtService.extractUsername(refreshToken);

        if (userEmail != null) {
            var user = this.userRepository.findByEmail(userEmail).orElseThrow();

            if (jwtService.isTokenValid(refreshToken, user)) {
                var accessToken = jwtService.generateToken(user);
                revokeAllUserTokens(user);
                saveUserToken(user, accessToken);
                var authResponse = AuthenticationResponse.builder().accessToken(accessToken).refreshToken(refreshToken).build();
                new ObjectMapper().writeValue(response.getOutputStream(), authResponse);
            }

        }
    }
}
