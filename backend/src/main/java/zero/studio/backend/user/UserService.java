package zero.studio.backend.user;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository repository;

    public void changePassword(ChangePasswordRequest request, Principal connectedUser) {

        var user = (User) ((UsernamePasswordAuthenticationToken) connectedUser).getPrincipal();

        // check if the current password is correct
        if (!passwordEncoder.matches(request.getCurrentPassword(), user.getPassword())) {
            throw new IllegalStateException("Wrong password");
        }
        // check if the two new passwords are the same
        if (!request.getNewPassword().equals(request.getConfirmationPassword())) {
            throw new IllegalStateException("Password are not the same");
        }

        // update the password
        user.setPassword(passwordEncoder.encode(request.getNewPassword()));

        // save the new password
        repository.save(user);
    }

    public UserDetailsResponse getUserDetails(Principal connectedUser) {
        var user = (User) ((UsernamePasswordAuthenticationToken) connectedUser).getPrincipal();

        return UserDetailsResponse
                .builder()
                .userId(user.getId())
                .username(user.getUsername())
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .email(user.getEmail())
                .role(user.getRole())
                .build();
    }

    @Transactional
    public Boolean updateUserDetails(Principal connectedUser, UserDetailsResponse userDetails) {
        var currentUser = (User) ((UsernamePasswordAuthenticationToken) connectedUser).getPrincipal();

        // Если такой пользователь имеется, но его username не равно текущему
        if (repository
                .findByUsername(userDetails.getUsername())
                .isPresent() &&
                !currentUser.getUsername().equals(userDetails.getUsername())
        ) {
            return false;
        }

        // Если такой пользователь имеется, но его email не равно текущему
        if (repository
                .findByEmail(userDetails.getEmail())
                .isPresent() &&
                !currentUser.getEmail().equals(userDetails.getEmail())
        ) {
            return false;
        }

        User user = repository.findByUsername(currentUser.getUsername()).orElseThrow();

        user.setEmail(userDetails.getEmail());
        user.setFirstname(userDetails.getFirstname());
        user.setLastname(userDetails.getLastname());
        user.setUsername(userDetails.getUsername());
        user.setEmail(userDetails.getEmail());

        repository.save(user);
        return true;
    }

    public Boolean isUserNameUsed(String askedUsername) {
        return repository.findByUsername(askedUsername).isPresent();
    }
}
