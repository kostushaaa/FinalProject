package zero.studio.backend.user;

import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;

    @PatchMapping("/change-password")
    public ResponseEntity<?> changePassword(
            @RequestBody ChangePasswordRequest request,
            Principal connectedUser
    ) {
        userService.changePassword(request, connectedUser);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/details")
    public ResponseEntity<UserDetailsResponse> getUserDetails(
            Principal connectedUser
    ) {
        return ResponseEntity.ok(userService.getUserDetails(connectedUser));
    }

    @PostMapping("/is-username-used")
    public ResponseEntity<?> isUserNameUsed(@RequestBody String askedUsername) {
        return ResponseEntity.ok(userService.isUserNameUsed(askedUsername));
    }

    @PostMapping("/update-user-details")
    public ResponseEntity<?> updateUserDetails(
            @RequestBody UserDetailsResponse userDetails,
            Principal connectedUser
    ) {

        if (userService.updateUserDetails(connectedUser, userDetails)) {
            return ResponseEntity.ok("User details updating has been succeed done");
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Such username or email are already used");
    }

}
