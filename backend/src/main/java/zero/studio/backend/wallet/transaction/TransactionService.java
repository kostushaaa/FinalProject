package zero.studio.backend.wallet.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zero.studio.backend.product.Product;
import zero.studio.backend.wallet.Wallet;
import zero.studio.backend.wallet.WalletRepository;
import zero.studio.backend.wallet.transaction.custom.Repository;

import java.util.List;
import java.util.UUID;

@Service
public class TransactionService {

    private final WalletRepository walletRepository;
    //    @Autowired
    private Repository repository = new Repository();

    public TransactionService(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    public List<Transaction> getLastPurchases(Integer qnt, UUID userId) {

        Wallet wallet = walletRepository.findByOwnerId(userId).orElseThrow();

        return repository.getPurchases(qnt, wallet.getId());
    }

    public List<Product> getMyPurchases(Integer qnt, UUID userId) {

        return repository.getMyPurchases(qnt, userId);

    }

}
