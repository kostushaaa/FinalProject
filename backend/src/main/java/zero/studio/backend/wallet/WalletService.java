package zero.studio.backend.wallet;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zero.studio.backend.user.UserRepository;
import zero.studio.backend.wallet.request.TransactionRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zero.studio.backend.wallet.transaction.Transaction;
import zero.studio.backend.wallet.transaction.TransactionRepository;
import zero.studio.backend.wallet.transaction.TransactionType;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class WalletService {

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    private static final Logger logger = LoggerFactory.getLogger(WalletService.class);

    @Transactional
    public double getBalance(UUID userId) {
        Wallet wallet = walletRepository.findByOwnerId(userId).orElseThrow();

        return wallet.getBalance();
    }


    @Transactional
    public List<Transaction> getHistory(UUID userId) {
        Wallet wallet = walletRepository.findByOwnerId(userId).orElseThrow();

        List<Transaction> transactions = new ArrayList<>();

        wallet.getReceivedTransactions().forEach(transaction -> {
            transactions.add(transaction);
        });

        wallet.getSentTransactions().forEach(transaction -> {
            transactions.add(transaction);
        });

        return transactions;
    }

    @Transactional
    public void magicChargeBalance(UUID initializedUserId, TransactionRequest request) {
        Wallet senderWallet = null;
        Wallet recipientWallet = null;


        senderWallet = walletRepository.findByOwnerId(initializedUserId).orElseThrow();

        recipientWallet = walletRepository.findByOwnerId(initializedUserId).orElseThrow();

        try {

            double recipientWalletBalance = recipientWallet.getBalance();
            recipientWallet.setBalance(recipientWalletBalance + request.getAmount());

            walletRepository.save(senderWallet);

            Transaction transaction = Transaction.builder()
                    .senderWallet(senderWallet)
                    .recipientWallet(recipientWallet)
                    .timestamp(LocalDateTime.now())
                    .amount(request.getAmount())
                    .isSuccessful(true)
                    .transactionType(TransactionType.RECHARGE)
                    .build();

            transactionRepository.save(transaction);

            logger.info("Транзакция успешно выполнена: пользователь {}, пользователю {} сумма {}", initializedUserId, request.getRecipientUserId(), request.getAmount());

        } catch (Exception e) {
            Transaction transaction = Transaction.builder()
                    .senderWallet(senderWallet)
                    .recipientWallet(recipientWallet)
                    .timestamp(LocalDateTime.now())
                    .amount(request.getAmount())
                    .isSuccessful(false)
                    .transactionType(TransactionType.RECHARGE)
                    .build();

            transactionRepository.save(transaction);

            logger.error("Ошибка при выполнении транзакции для пользователя {}", initializedUserId, e);
            throw e;
        }
    }

    @Transactional
    public boolean sendMoney(UUID senderId, TransactionRequest request) {
        Wallet senderWallet = null;
        Wallet recipientWallet = null;
        if (senderId.equals(request.getRecipientUserId())) {
            return false;
        }
        try {
            senderWallet = walletRepository.findByOwnerId(senderId).orElseThrow();
            recipientWallet = walletRepository.findByOwnerId(request.getRecipientUserId()).orElseThrow();

            double senderWalletBalance = senderWallet.getBalance();
            double recipientWalletBalance = senderWallet.getBalance();

            if (request.getAmount() > senderWalletBalance) {
                logger.error("Insufficient funds: The requested amount exceeds the sender's wallet balance");
                return false;
            }

            senderWallet.setBalance(senderWalletBalance - request.getAmount());

            recipientWallet.setBalance(recipientWalletBalance + request.getAmount());

            walletRepository.save(senderWallet);
            walletRepository.save(recipientWallet);

            Transaction transaction = Transaction.builder()
                    .senderWallet(senderWallet)
                    .recipientWallet(recipientWallet)
                    .timestamp(LocalDateTime.now())
                    .amount(request.getAmount())
                    .isSuccessful(true)
                    .transactionType(TransactionType.TRANSFER)
                    .build();

            transactionRepository.save(transaction);

            logger.info("Транзакция успешно выполнена: пользователь {}, сумма {}", request.getRecipientUserId(), request.getAmount());

        } catch (Exception e) {
            Transaction transaction = Transaction.builder()
                    .senderWallet(senderWallet)
                    .recipientWallet(recipientWallet)
                    .timestamp(LocalDateTime.now())
                    .amount(request.getAmount())
                    .isSuccessful(false)
                    .transactionType(TransactionType.TRANSFER)
                    .build();

            transactionRepository.save(transaction);

            logger.error("Ошибка при выполнении транзакции для пользователя {}", request.getRecipientUserId(), e);
        }
        return true;
    }


}
