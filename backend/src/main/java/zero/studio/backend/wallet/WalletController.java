package zero.studio.backend.wallet;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import zero.studio.backend.user.User;
import zero.studio.backend.wallet.request.TransactionRequest;

import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/api/wallet")
@RequiredArgsConstructor
public class WalletController {

    @Autowired
    private WalletService walletService;

    @PostMapping("/charge")
    public ResponseEntity<?> chargeBalance(@RequestBody(required = true) TransactionRequest chargeBalanceRequest,
                                           Principal connectedUser) {
        var user = (User) ((UsernamePasswordAuthenticationToken) connectedUser).getPrincipal();

        try {
            walletService.magicChargeBalance(user.getId(), chargeBalanceRequest);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to process the money transaction");
        }

        return ResponseEntity.ok("The balance has been successfully replenished");
    }

    @PostMapping("/transfer-money")
    public ResponseEntity<?> transferMoney(@RequestBody(required = true) TransactionRequest chargeBalanceRequest,
                                           Principal connectedUser) {
        var user = (User) ((UsernamePasswordAuthenticationToken) connectedUser).getPrincipal();

        if( walletService.sendMoney(user.getId(), chargeBalanceRequest) ) {
            return ResponseEntity.ok("The money transaction has been successfully done");
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to process the money transaction");
    }

    @GetMapping("/get-wallet-details")
    public ResponseEntity<?> getBalance(Principal connectedUser) {
        var user = (User) ((UsernamePasswordAuthenticationToken) connectedUser).getPrincipal();
        return ResponseEntity.ok(walletService.getBalance(user.getId()));
    }

    @GetMapping("/get-transactions-history")
    public ResponseEntity<?> getHistory(Principal connectedUser) {
        var user = (User) ((UsernamePasswordAuthenticationToken) connectedUser).getPrincipal();
        return ResponseEntity.ok(walletService.getHistory(user.getId()));
    }

    @PostMapping("/test")
    public ResponseEntity<?> test(@RequestBody(required = true) Map<String, Double> cnt,
                                  Principal connectedUser) {
        return ResponseEntity.ok(cnt.get("cnt"));
    }

}
