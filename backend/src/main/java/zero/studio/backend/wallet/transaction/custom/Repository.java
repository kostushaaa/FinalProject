package zero.studio.backend.wallet.transaction.custom;

import org.jooq.*;
import org.jooq.Record;
import org.jooq.impl.DSL;
import zero.studio.backend.config.jooq.JooqConfiguration;
import zero.studio.backend.product.Product;
import zero.studio.backend.product.ProductTypes;
import zero.studio.backend.wallet.transaction.Transaction;
import zero.studio.backend.wallet.transaction.TransactionType;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.jooq.impl.DSL.*;

public class Repository {

    private static final JooqConfiguration jooqConfiguration = new JooqConfiguration("jdbc:postgresql://localhost:5432/ventorspace_bd", "ventorspace_adept", "secretpassword");
    private DSLContext ctx;


    public List<Transaction> getPurchases(Integer quantity, UUID walletId) {
        try {
            ctx = jooqConfiguration.dsl();

            Result<Record4<UUID, String, Double, Timestamp>> result = ctx
                    .select(
                            field(name("id"), UUID.class),
                            field(name("transaction_type"), String.class),
                            field(name("amount"), Double.class),
                            field(name("timestamp"), Timestamp.class)
                    )
                    .from(table(name("transactions")))
                    .where(
                            field(name("recipient_wallet_id"))
                                    .eq(walletId)
                                    .and(
                                            field(name("is_successful"))
                                                    .eq(true)
                                    )
                    )
                    .orderBy(field(name("timestamp")).asc())
                    .limit(quantity)
                    .fetch();

            List<Transaction> response = new ArrayList<>();

            result.forEach(res -> {
                Transaction transaction = Transaction
                        .builder()
                        .id(res.value1())
                        .transactionType(TransactionType.valueOf(res.value2()))
                        .timestamp(res.value4().toLocalDateTime())
                        .amount(res.value3())
                        .isSuccessful(true)
                        .build();

                response.add(transaction);
            });

            return response;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public List<Product> getMyPurchases(Integer qnt, UUID userId) {
        try {
            ctx = jooqConfiguration.dsl();

            String sql = "SELECT products.id,\n" +
                    "       products.price,\n" +
                    "       products.description,\n" +
                    "       products.name,\n" +
                    "       products.product_types,\n" +
                    "       product_photos.photos\n" +
                    "\n" +
                    "FROM products\n" +
                    "         right join transactions_items\n" +
                    "                    on public.products.id = public.transactions_items.items_id\n" +
                    "         right join transactions\n" +
                    "                    on public.transactions_items.transaction_id = public.transactions.id\n" +
                    "         right join wallet\n" +
                    "                    on public.transactions.sender_wallet_id = wallet.id\n" +
                    "         right join product_photos\n" +
                    "                    on products.id = product_photos.product_id\n" +
                    "where public.wallet.user_id = '" + userId + "'\n" +
                    "  and public.transactions.transaction_type = 'PURCHASE'\n" +
                    "order by transactions.timestamp desc\n" +
                    "limit " + qnt + ";\n";

            Result<Record> result = ctx.fetch(sql);

            List<Product> response = new ArrayList<>();

            result.forEach(res -> {
                List<String> list = new ArrayList<>();
                list.add(res.getValue("photos", String.class));


                Product product = Product.builder()
                        .id(res.getValue("id", UUID.class))
                        .price(res.getValue("price", Double.class))
                        .name(res.getValue("name", String.class))
                        .photos(list)
                        .description(res.getValue("description", String.class))
                        .productTypes(ProductTypes.valueOf(res.getValue("product_types", String.class)))
                        .build();

                response.add(product);
            });

            return response;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public List<Product> getProducts(int quantity, int offset) {
        try {
            ctx = jooqConfiguration.dsl();

            String sql = "\n" +
                    "SELECT products.id,\n" +
                    "       products.price,\n" +
                    "       products.description,\n" +
                    "       products.name,\n" +
                    "       products.product_types,\n" +
                    "       product_photos.photos\n" +
                    "FROM products\n" +
                    "right join product_photos on products.id = product_photos.product_id\n" +
                    "limit " + quantity + " offset " + offset + ";";


            Result<Record> result = ctx.fetch(sql);

            List<Product> response = new ArrayList<>();

            result.forEach(res -> {
                List<String> list = new ArrayList<>();
                list.add(res.getValue("photos", String.class));


                Product product = Product.builder()
                        .id(res.getValue("id", UUID.class))
                        .price(res.getValue("price", Double.class))
                        .name(res.getValue("name", String.class))
                        .photos(list)
                        .description(res.getValue("description", String.class))
                        .productTypes(ProductTypes.valueOf(res.getValue("product_types", String.class)))
                        .build();

                response.add(product);
            });

            return response;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
