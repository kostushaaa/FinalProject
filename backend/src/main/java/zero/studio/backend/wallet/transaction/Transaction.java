package zero.studio.backend.wallet.transaction;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ManyToAny;
import zero.studio.backend.product.Product;
import zero.studio.backend.wallet.Wallet;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@Entity
@Table(name = "transactions")
public class Transaction {
    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "sender_wallet_id", nullable = false)
    private Wallet senderWallet;

    @ManyToOne
    @JoinColumn(name = "recipient_wallet_id", nullable = false)
    private Wallet recipientWallet;

    @Column(nullable = false)
    private LocalDateTime timestamp;

    @Column(nullable = false)
    private Double amount;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isSuccessful;

    @Column(nullable = true)
    @ManyToAny
    private List<Product> items;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    public Transaction(UUID id, Wallet senderWallet, Wallet recipientWallet, LocalDateTime timestamp, Double amount, Boolean isSuccessful, List<Product> items, TransactionType transactionType) {
        this.id = id;
        this.senderWallet = senderWallet;
        this.recipientWallet = recipientWallet;
        this.timestamp = timestamp;

        // Проверяем, что значение amount не отрицательное
        if (amount < 0) {
            throw new IllegalArgumentException("Amount cannot be negative");
        }

        this.amount = amount;
        this.isSuccessful = isSuccessful;
        this.items = items;
        this.transactionType = transactionType;
    }

}
