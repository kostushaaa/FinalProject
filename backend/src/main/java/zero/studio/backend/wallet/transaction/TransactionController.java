package zero.studio.backend.wallet.transaction;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import zero.studio.backend.user.User;

import java.security.Principal;

@RestController
@RequestMapping("/api/transaction")
@RequiredArgsConstructor
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @GetMapping("/get-last-purchases")
    public ResponseEntity<?> getLastPurchases(@RequestParam(name = "qnt", defaultValue = "10") int quantity, Principal connectedUser) {
        try {
            var user = (User) ((UsernamePasswordAuthenticationToken) connectedUser).getPrincipal();

            System.out.println("Do i receive it right ?\n" + quantity + " for user with userId: " + user.getId());

            return ResponseEntity.ok(transactionService.getLastPurchases(quantity, user.getId()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Request getLastPurchases went off bad");
        }
    }

}
