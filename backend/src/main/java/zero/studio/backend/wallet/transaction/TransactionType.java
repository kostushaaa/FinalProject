package zero.studio.backend.wallet.transaction;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TransactionType {
    TRANSFER, // Перевод
    RECHARGE, // Пополнение
    PURCHASE, // Покупка

}
