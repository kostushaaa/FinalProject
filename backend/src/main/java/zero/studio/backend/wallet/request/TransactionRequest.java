package zero.studio.backend.wallet.request;

import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import zero.studio.backend.wallet.transaction.TransactionType;

import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequest {

    @NotNull
    private double amount;

    @Nullable
    private UUID recipientUserId; // идентификатор получателя

    @Nullable
    private String username;

    @Nullable
    private String email;

}
