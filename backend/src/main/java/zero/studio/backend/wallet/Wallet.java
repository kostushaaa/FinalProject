package zero.studio.backend.wallet;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import zero.studio.backend.user.User;
import zero.studio.backend.wallet.transaction.Transaction;

import java.util.Currency;
import java.util.List;
import java.util.UUID;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "wallet")
public class Wallet {
    @Id
    @GeneratedValue
    private UUID id;

    @OneToOne
    @JoinColumn(name = "user_id", unique = true, nullable = false)
    private User owner;

    @OneToMany(mappedBy = "senderWallet", cascade = CascadeType.ALL)
    private List<Transaction> sentTransactions;

    @OneToMany(mappedBy = "recipientWallet", cascade = CascadeType.ALL)
    private List<Transaction> receivedTransactions;


    @NotNull
    private Double balance;

    private Currency dollar;

    @Override
    public String toString() {
        return "Wallet{" +
                "id=" + id +
                ", owner=" + owner +
                ", balance=" + balance +
                ", dollar=" + dollar +
                '}';
    }
}
