package zero.studio.backend.gamenews.review;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zero.studio.backend.gamenews.game.Game;
import zero.studio.backend.gamenews.image.Image;
import zero.studio.backend.gamenews.release.Release;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "reviews")
public class Review {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date publish_date;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date update_date;
    private String review_type;
    @Id
    private Long id;
    private String authors;
    private String title;

    @ManyToOne
    private Image image;

    private double score;
    private String deck;
    private String good;
    private String bad;
    private String body;
    private String lede;

    @ManyToOne
    private Game game;

    @OneToMany(mappedBy = "review", cascade = CascadeType.ALL)
    private List<Release> releases;

    private String site_detail_url;
}
