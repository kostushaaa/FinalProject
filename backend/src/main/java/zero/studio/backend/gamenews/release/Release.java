package zero.studio.backend.gamenews.release;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zero.studio.backend.gamenews.review.Review;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "releases")
public class Release {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Review review;

    private String upc;
    private String distribution_type;
    private String name;
    private String region;
    private String platform;
    private String api_detail_url;
}
