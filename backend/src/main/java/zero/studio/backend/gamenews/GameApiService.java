package zero.studio.backend.gamenews;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import zero.studio.backend.gamenews.game.GameRepository;
import zero.studio.backend.gamenews.image.ImageRepository;
import zero.studio.backend.gamenews.release.ReleaseRepository;
import zero.studio.backend.gamenews.review.Review;
import zero.studio.backend.gamenews.review.ReviewRepository;
import zero.studio.backend.gamenews.review.ReviewResponse;

import java.util.List;

@Service
public class GameApiService {

    @Value("${newsApi.key}")
    private String apiKey;

    @Value("${newsApi.url}")
    private String baseUrl;

    public List<Review> getReviews(int qnt) {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = baseUrl + "reviews/?api_key=" + apiKey + "&limit=" + qnt + "&format=json";

        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", "Java spring some staff");
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<ReviewResponse> response = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, ReviewResponse.class);

        ReviewResponse reviewResponse = response.getBody();

        if (reviewResponse != null && reviewResponse.getResults() != null) {
            return reviewResponse.getResults();
        } else {
            System.out.println("No reviews found in the API response.");
            return null;
        }
    }

    public Review getReviewById(int id) {
        RestTemplate restTemplate = new RestTemplate();

        String apiUrl = baseUrl + "reviews/?api_key=" + apiKey + "&id=" + id + "&format=json";

        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", "Java spring some staff");
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<ReviewResponse> response = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, ReviewResponse.class);

        ReviewResponse reviewResponse = response.getBody();

        if (reviewResponse != null && reviewResponse.getResults() != null) {
            return reviewResponse.getResults().get(0);
        } else {
            System.out.println("No reviews found in the API response.");
            return null;
        }
    }

}
