package zero.studio.backend.gamenews;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/news")
public class GameNewsController {

    @Autowired
    private GameApiService gameApiService;

    @GetMapping("/getReviews")
    public ResponseEntity<?> loadNews(@RequestParam(name = "newsQNT") Integer newsQNT) {
        Object result = gameApiService.getReviews(newsQNT);

        if (result == null) {
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("News service in the world of games is currently unavailable");
        } else {
            return ResponseEntity.ok(result);
        }
    }

    @GetMapping("/getReview")
    public ResponseEntity<?> getReviewById(@RequestParam(name = "reviewId") int id) {
        return ResponseEntity.ok(gameApiService.getReviewById(id));
    }


}
