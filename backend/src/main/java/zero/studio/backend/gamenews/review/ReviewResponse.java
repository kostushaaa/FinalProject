package zero.studio.backend.gamenews.review;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReviewResponse {
    @JsonProperty("error")
    private String error;

    @JsonProperty("limit")
    private int limit;

    @JsonProperty("offset")
    private int offset;

    @JsonProperty("number_of_page_results")
    private int numberOfPageResults;

    @JsonProperty("number_of_total_results")
    private int numberOfTotalResults;

    @JsonProperty("results")
    private List<Review> results;
}
