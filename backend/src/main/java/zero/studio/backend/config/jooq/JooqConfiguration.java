package zero.studio.backend.config.jooq;

import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;

public class JooqConfiguration extends DefaultConfiguration {

    public JooqConfiguration(String jdbcUrl, String username, String password) {
        set(SQLDialect.POSTGRES);
        set(new DataSourceConnectionProvider(new JooqDataSource(jdbcUrl, username, password)));
    }
}
