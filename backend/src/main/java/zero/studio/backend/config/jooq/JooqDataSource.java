package zero.studio.backend.config.jooq;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class JooqDataSource extends DriverManagerDataSource {

    public JooqDataSource(String jdbcUrl, String username, String password) {
        super();
        setUrl(jdbcUrl);
        setUsername(username);
        setPassword(password);
    }
}
