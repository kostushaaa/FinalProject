package zero.studio.backend.product;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue
    private UUID id;

    private String name;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @Builder.Default
    private ProductTypes productTypes = ProductTypes.Game;

    @ElementCollection
    private List<String> photos;

    private String description;

    private double price;

    public Product(UUID id, String name, double price) {
        this.id = id;
        this.name = name;
        if (price < 0) {
            throw new IllegalArgumentException("Value must be non-negative.");
        }
        this.price = price;
    }

    public Product(double value) {
        if (value < 0) {
            throw new IllegalArgumentException("Value must be non-negative.");
        }
        this.price = value;
    }

    public void setPrice(double value) {
        if (value < 0) {
            throw new IllegalArgumentException("Value must be non-negative.");
        }
        this.price = value;
    }


}
