package zero.studio.backend.product;

import jakarta.transaction.Transactional;
import zero.studio.backend.user.User;
import org.springframework.stereotype.Service;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import zero.studio.backend.user.UserRepository;
import zero.studio.backend.wallet.Wallet;
import zero.studio.backend.wallet.WalletRepository;
import zero.studio.backend.wallet.WalletService;
import zero.studio.backend.wallet.transaction.Transaction;
import zero.studio.backend.wallet.transaction.TransactionRepository;
import zero.studio.backend.wallet.transaction.TransactionType;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    private final WalletService walletService;
    private final WalletRepository walletRepository;
    private final TransactionRepository transactionRepository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    public ProductService(WalletService walletService, WalletRepository walletRepository, TransactionRepository transactionRepository, ProductRepository productRepository, UserRepository userRepository) {
        this.walletService = walletService;
        this.walletRepository = walletRepository;
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public Boolean buyItem (Principal connectedUser, Product product) {
        var currentUser = (User) ((UsernamePasswordAuthenticationToken) connectedUser).getPrincipal();

        User admin = userRepository.findByUsername("admin228").orElseThrow();

        Wallet wallet = null;
        Wallet adminWallet = null;

        wallet = walletRepository.findByOwnerId(currentUser.getId()).orElseThrow();
        adminWallet = walletRepository.findByOwnerId(admin.getId()).orElseThrow();

        try {
            Double walletBalance = walletService.getBalance(currentUser.getId());

            if (product.getPrice() > walletBalance) {
                return false; // Недостаточно средств на счету
            }

            wallet.setBalance(walletBalance - product.getPrice());

            walletRepository.save(wallet);
            List<Product> products = new ArrayList<>();
            products.add(product);

            Transaction transaction = Transaction.builder()
                    .senderWallet(wallet)
                    .recipientWallet(adminWallet)
                    .timestamp(LocalDateTime.now())
                    .amount(product.getPrice())
                    .isSuccessful(true)
                    .transactionType(TransactionType.PURCHASE)
                    .items(products)
                    .build();

            transactionRepository.save(transaction);


            return true;
        } catch (Exception e) {
            Transaction transaction = Transaction.builder()
                    .senderWallet(wallet)
                    .recipientWallet(adminWallet)
                    .timestamp(LocalDateTime.now())
                    .amount(product.getPrice())
                    .isSuccessful(false)
                    .transactionType(TransactionType.PURCHASE)
                    .build();


            transactionRepository.save(transaction);

            e.printStackTrace();
        }

        return false;
    }


    public boolean createItem(Product product) {
        try {
            Product createProduct = Product
                    .builder()
                    .name(product.getName())
                    .description(product.getDescription())
                    .price(product.getPrice())
                    .photos(product.getPhotos())
                    .build();

            productRepository.save(createProduct);
        } catch (Exception e) {
            return false;
        }
//
        return true;
    }
}
