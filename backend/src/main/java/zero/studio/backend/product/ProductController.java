package zero.studio.backend.product;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import zero.studio.backend.user.User;

import zero.studio.backend.wallet.transaction.custom.Repository;

import java.security.Principal;
import java.util.List;
import java.util.Arrays;
import java.util.UUID;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final ProductRepository productRepository;
    private static final Repository repository = new Repository();

    @PostMapping("/buy-item")
    public ResponseEntity<String> buyItem(
            @RequestBody Product product,
            Principal connectedUser
    ) {
        if (productService.buyItem(connectedUser, product)) {
            return ResponseEntity.ok("The product has been successfully bought");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to process the money transaction");
        }
    }

    @PostMapping("/create-product")
    public ResponseEntity<String> createProduct(
            @RequestBody Product product
    ) {
        try {
            productService.createItem(product);
            return ResponseEntity.ok("Product have been successfully created");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to create the product");
        }
    }

    @PostMapping("/create-products")
    public ResponseEntity<String> createProducts(@RequestBody List<Product> products) {
        try {
            for (Product product : products) {
                productService.createItem(product);
            }
            return ResponseEntity.ok("Products have been successfully created");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to create the products");
        }
    }

    @GetMapping("/{productId}")
    public ResponseEntity<Product> getProductById(@PathVariable UUID productId) {
        try {
            Product product = productRepository.findById(productId).orElseThrow();
            return ResponseEntity.ok(product);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/get-products")
    public ResponseEntity<List<Product>> getProducts(
            @RequestParam(name = "qnt", defaultValue = "10") int quantity,
            @RequestParam(name = "offset", defaultValue = "0") int offset) {
        try {
            return ResponseEntity.ok(repository.getProducts(quantity, offset));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GetMapping("/get-products-types")
    public ResponseEntity<?> getProductsTypes() {
        List<ProductTypes> productTypes = Arrays.asList(ProductTypes.values());
        return ResponseEntity.ok(productTypes);
    }


    @GetMapping("/get-my-purchases")
    public ResponseEntity<?> getMyPurchases(
            @RequestParam(name = "qnt", defaultValue = "10") int quantity,
            Principal connectedUser
    ) {
        var currentUser = (User) ((UsernamePasswordAuthenticationToken) connectedUser).getPrincipal();

        try {
            return ResponseEntity.ok(repository.getMyPurchases(quantity, currentUser.getId()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
