package zero.studio.backend.demo;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/public")
//@PreAuthorize("hasRole('ADMIN')")
@Hidden
public class DemoController {

  public static Integer AP_IP;
  public static String AP_PASSWORD;
  public static String AP_SSID;
  public static Integer HTTP_PORT;
  public static String PASSWORD;
  public static String SSID;
  @GetMapping("/get-current-settings")
  public ResponseEntity<?> test2() {
    String response = "SSID: " + this.SSID + ", PASSWORD: " + this.PASSWORD + ", HTTP_PORT: " + this.HTTP_PORT +
            ", AP_SSID: " + this.AP_SSID + ", AP_PASSWORD: " + this.AP_PASSWORD + ", AP_IP: " + this.AP_IP;
    return ResponseEntity.ok(response);
  }

  @PostMapping("/update-settings")
  public ResponseEntity<?> test(@RequestParam(name = "AP_IP") Integer AP_IP,
                                @RequestParam(name = "AP_PASSWORD") String AP_PASSWORD,
                                @RequestParam(name = "AP_SSID") String AP_AP_SSID,
                                @RequestParam(name = "HTTP_PORT") Integer HTTP_PORT,
                                @RequestParam(name = "PASSWORD") String PASSWORD,
                                @RequestParam(name = "SSID") String SSID
                                ) {

    this.AP_IP = AP_IP;
    this.AP_PASSWORD = AP_PASSWORD;
    this.AP_SSID = AP_AP_SSID;
    this.HTTP_PORT = HTTP_PORT;
    this.PASSWORD = PASSWORD;
    this.SSID = SSID;
    return ResponseEntity.ok("Settings updated successfully");
  }

}
