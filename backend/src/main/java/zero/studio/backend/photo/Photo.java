package zero.studio.backend.photo;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name= "photos")
public class Photo {

    @Id
    @GeneratedValue
    private UUID id;

    @Lob
    private byte[] photoData;

    private String photoName;

    private Date uploadDate;

}
