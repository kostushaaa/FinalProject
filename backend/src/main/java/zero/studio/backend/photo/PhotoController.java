package zero.studio.backend.photo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/public")
public class PhotoController {

    @Autowired
    private PhotoService photoService;

    @PostMapping("/upload-photo")
    public ResponseEntity<?> uploadedProfilePhoto(@RequestParam("file") MultipartFile file) {
        try {
            Photo photo = photoService.savePhoto(file);


            String photoURL = "/api/public/profile/photo/" + photo.getId();
            return ResponseEntity.ok(photoURL);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to upload profile photo.");
        }
    }

    @GetMapping("/photo/{photoId}")
    public ResponseEntity<byte[]> getProfilePhoto(@PathVariable UUID photoId) {
        Photo photo = photoService.getPhoto(photoId);
        if (photo != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_JPEG); // Устанавливаем тип контента изображения
            return new ResponseEntity<>(photo.getPhotoData(), headers, HttpStatus.OK);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping("/hello")
    public String hello() {
        return "Hello";
    }

}
