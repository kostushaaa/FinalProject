package zero.studio.backend.photo;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PhotoService {

    @Autowired
    private PhotoRepository photoRepository;

    public Photo savePhoto(MultipartFile file) throws IOException {

        Photo photo = new Photo();
        photo.setPhotoName(file.getOriginalFilename());
        photo.setUploadDate(new Date());
        photo.setPhotoData(file.getBytes());

        return photoRepository.save(photo);
    }

    public Photo getPhoto(UUID id) {
        return photoRepository.findById(id).orElse(null);
    }

}
