package zero.studio.backend;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import zero.studio.backend.auth.AuthenticationService;
import zero.studio.backend.auth.RegisterRequest;
import zero.studio.backend.user.Role;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:5173", "http://127.0.0.1:5173"})
public class KostiaAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(KostiaAuthApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(
            AuthenticationService service
    ) {
        return args -> {
            var admin = RegisterRequest.builder()
                    .username("admin228")
                    .email("admin@mail.com")
                    .password("password")
                    .role(Role.ADMIN)
                    .build();
            System.out.println("Admin token: " + service.register(admin).getAccessToken());

            var manager = RegisterRequest.builder()
                    .username("manager112")
                    .email("manager@mail.com")
                    .password("password")
                    .role(Role.MANAGER)
                    .build();
            System.out.println("Manager token: " + service.register(manager).getAccessToken());

            var adminMjkurwa = RegisterRequest.builder()
                    .username("mjkurwa")
                    .email("mjkurwa@gmail.com")
                    .password("22813371")
                    .role(Role.ADMIN)
                    .build();
            System.out.println("adminMjkurwa token: " + service.register(adminMjkurwa).getAccessToken());

        };
    }

//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurer() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/api").allowedOrigins("http://localhost:5173", "http://localhost:8081");
//            }
//        };
//    }
}
